# W.T.C.E.

I once asked myself, what is the simplest CMS I can make, and this is what I came up with. If you can edit text, you can use it. If you want your web page to look good, you need to know how HTML and CSS works.

This software is used to generate https://lighthouse.1a-insec.net/.

> **Important**  
> Remember to backup your CSV files frequently! This piece of software **will** eat your data. I recommend using git for this purpose.

![screenshot](screenshot.png)

## How Does It Work

- You store some data in a CSV file.
- You can modify the data by editing the CSV file.
- A web server loads data from the CSV files and renders a web page from an HTML template (`page-view.ts`).
- When you edit a piece of text on the page, the corresponding cell in the CSV file is updated.
- When you are done editing, you can save the rendered page and use it somewhere else.

## Usage

We now explain how to edit CSV files with this tool.

By now you should have this repo on your comuputer and looking at the local copy of `readme.md` inside that repo. If not, clone the repo now.

Now, look inside `deno.json`. You will see two copies of the string `page_view.ts data.csv`. The first argument is the **page view file**, and the rest are CSV files to edit. You can specify multiple CSV files to edit by setting the arguments `page_view.ts data0.csv data1.csv data2.csv` in `deno.json`.

Now, start the web interface with `deno task dev` and open the http:// link in your browser. You should see a web interface for editing rows of the CSV file(s).

### Modifying Columns

To do that, edit the first line of the CSV file.

### Adding or Deleting Rows
To do that, edit the content of the CSV file.

To delete a row, delete the line.

To add a row, add a new line containing a single comma (`,`).

### Modifying Rows
This is done with the web interface. Click on a text field and edit it. Changes are saved automatically.

`editing: true` will be passed to `IndexPage` in `page_view.ts`.

And of course, you can still edit the CSV file by hand.

### Rendering The Page

It's as simple as `deno task render`.

`editing: false` will be passed to `IndexPage` in `page_view.ts`.

If you know programming, you can edit `page_view.ts` to show different stuff when `editing` is `true` or `false`.

### Publishing The Website

To publish the website on pages.sr.ht, use the following command:

```sh
deno task render > public/index.html && hut pages publish public -d 'example.com'
```

If you use rsync, then run:

```sh
deno task render > public/index.html && rsync -a --delete public/ user@remote:dest/
```

Other publishing tools work more or less the same.

## HTTP Routes In Dev Mode

Routes defined in the HTTP Server started by `deno task dev`.

### `/`

On this route, the server will serve the web interface defined in `page_view.ts`.

### `/edit.js`

On this route, the server will serve a bundled version of `src/edit.ts`. This is the script that makes the page editable.

### `/*`

The rest of the routes serve files from the `public/` directory.

For example, visiting the URL `/abc` will give you the file `public/abc`.

## Templating

The page sent to the browser should contain structures like this:

```html
<div data-file="data.csv">
  <div data-row="0">
    <div data-column="id">1234</div>
  </div>
</div>
```

This is what `src/edit.ts` will recognize and work with. The elements don't have to be direct parent/child, as long they are nested in the correct order: `data-file`, `data-row`, `data-column`.

Look inside `page_view.ts`. The structure is already set up for you.

## Common CSV Database Patterns

In case you haven't used spreadsheet before, here are some ways you can format your CSV files.

### Key-Value

```csv
key,value
a,b
c,d
```

### Numbered Tuples

```csv
id,0,1,2
:0,:iacore,:says,"hi"
:1,:0,:date,"2024-05-14"
```

### Long-Form Table

```csv
sepal length,sepal width,petal length,petal width,species
5.1,3.5,1.4,0.2,Iris-setosa
4.9,3.0,1.4,0.2,Iris-setosa
```
