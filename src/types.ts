export type UpdateEventData = {
  dataset: string; // filename
  index: number;
  key: string; // insurance
  value: string; // insurance
  changed_key: string;
  changed_value: string;
};
