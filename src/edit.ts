import type { UpdateEventData } from "./types.ts";

document.querySelectorAll("[data-file]").forEach((elDataset) => {
  const dataset_name = elDataset.getAttribute("data-file");
  if (!dataset_name) {
    console.warn("element has empty [data-file]", elDataset);
    return;
  }
  elDataset.querySelectorAll("[data-row]").forEach((el) => {
    const index_s = el.getAttribute("data-row");
    if (index_s === null || index_s === "") {
      console.warn("element has empty [data-row]", el);
      return;
    }
    const index = Number(index_s);

    let elField_key: HTMLElement | undefined = undefined;
    const elements: HTMLElement[] = [];
    el.querySelectorAll("[data-column]").forEach((_elField) => {
      const elField = _elField as HTMLElement;
      if (!elField.getAttribute("data-column")) {
        console.warn("element has empty [data-column]", elField);
        return;
      }
      if (elField.hasAttribute("data-key") || !elField_key) {
        elField_key = elField;
      }
      elField.contentEditable = "true";
      elements.push(elField);
    });
    if (elField_key === undefined) {
      console.warn(
        "element with [data-row] has no child with [data-column]",
        el,
      );
      return;
    }
    let elField_key_key: string, elField_key_value: string;
    const update_cached_key = () => {
      elField_key_key = elField_key!.getAttribute("data-column") ?? "";
      elField_key_value = elField_key!.textContent ?? "";
    };
    update_cached_key();
    if (elements.length == 0) {
      console.warn("element with [data-row] has no field withstanding", el);
    }
    elements.forEach((elField) => {
      elField.addEventListener("input", (_e) => {
        fetch("/update-object", {
          method: "POST",
          body: JSON.stringify(
            {
              dataset: dataset_name,
              index: index,
              key: elField_key_key,
              value: elField_key_value,
              changed_key: elField.getAttribute("data-column") ?? "",
              changed_value: elField.textContent ?? "",
            } satisfies UpdateEventData,
          ),
        });
        update_cached_key();
      });
    });
  });
});
