import render from "https://esm.sh/preact-render-to-string@6.4.2";
import { readCSVDataMulti } from "./csv.ts";
import { get_config, import_page_view } from "./config.ts";

const config = await get_config();
const { doctype, IndexPage } = await import_page_view(config.page_view);
const dataset = await readCSVDataMulti(config.data_files);
const body = (doctype ?? "<!DOCTYPE html>") +
  render(IndexPage({ dataset, editing: false }));

const encoder = new TextEncoder();

await Deno.stdout.write(encoder.encode(body));
