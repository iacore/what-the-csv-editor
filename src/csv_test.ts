import { assertEquals } from "jsr:@std/assert";
import { readCSVData, writeCSVData } from "./csv.ts";

const exampleData = {
  columns: ["a", "b", "c"],
  objects: [{ a: "1", b: "2", c: "" }],
};

const test_file = "csv_test_example.csv";

Deno.test(async function read() {
  assertEquals(await readCSVData(test_file, {}), exampleData);
});

Deno.test(async function write() {
  await writeCSVData(test_file, exampleData);
  assertEquals(await readCSVData(test_file, {}), exampleData);
});
