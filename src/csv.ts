import {
  type CommonCSVReaderOptions,
  type CSVWriteCellOptions,
  type CSVWriterOptions,
  readCSV,
  writeCSV,
} from "https://deno.land/x/csv@v0.9.3/mod.ts";

export type ParsedCSVData = {
  columns: string[];
  objects: Record<string, string>[];
};

const zip = <A, B>(a: A[], b: B[]) => a.map((k, i) => [k, b[i]]);

export async function readCSVDataMulti(
  filenames: string[],
  options?: Partial<CommonCSVReaderOptions>,
): Promise<Record<string, ParsedCSVData>> {
  const loaded = await Promise.all(
    filenames.map((s) => readCSVData(s, options)),
  );
  return Object.fromEntries(zip(filenames, loaded));
}
export async function readCSVData(
  filename: string,
  options?: Partial<CommonCSVReaderOptions>,
): Promise<ParsedCSVData> {
  let data;

  async function readRows(filename: string) {
    let data = [];

    const f = await Deno.open(filename);

    for await (const row of readCSV(f, options)) {
      const data_row = [];
      for await (const cell of row) {
        data_row.push(cell);
      }
      data.push(data_row);
    }

    f.close();

    return data;
  }

  try {
    data = await readRows(filename);
  } catch (e) {
    if (e instanceof Deno.errors.NotFound) {
      try {
        data = await readRows(filename + "~");
      } catch (e) {
        if (e instanceof Deno.errors.NotFound) {
          return {
            columns: [],
            objects: [],
          };
        } else {
          throw e;
        }
      }
    } else {
      throw e;
    }
  }

  if (data.length == 0) {
    return {
      columns: [],
      objects: [],
    };
  } else {
    const objects: Record<string, string>[] = [];
    const columns = data[0];
    data.slice(1).forEach((row) => {
      const obj: Record<string, string> = {};
      columns.forEach((col, i) => {
        if (i < row.length) {
          obj[col] = row[i];
        } else {
          obj[col] = "";
        }
      });
      objects.push(obj);
    });
    return {
      columns,
      objects,
    };
  }
}

export async function writeCSVData(
  filename: string,
  data: ParsedCSVData,
  options?: Partial<CSVWriterOptions & CSVWriteCellOptions>,
) {
  const rows = [data.columns];
  data.objects.forEach((o) => {
    rows.push(data.columns.map((col) => o[col]));
  });

  const f = await Deno.open(filename + "~", {
    write: true,
    create: true,
    truncate: true,
  });
  await writeCSV(f, rows, options);

  // await f.sync();
  try {
    await Deno.remove(filename);
  } catch (e) {
    if (e instanceof Deno.errors.NotFound) {
      // do nothing
    } else {
      throw e;
    }
  }
  await Deno.link(filename + "~", filename); // race condition monster please spare me, let me link a fd directly!
  f.close();
  await Deno.remove(filename + "~");
}
