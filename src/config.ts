export type Config = {
  public_fsRoot: string;

  page_view: string;
  data_files: string[];
};

import { Command } from "https://deno.land/x/cliffy@v1.0.0-rc.4/command/mod.ts";

export async function get_config(): Promise<Config> {
  const parsed_input = await new Command()
    .arguments("<page_view.tsx:string> [...data.csv:string]")
    .parse(Deno.args);

  return {
    data_files: parsed_input.args.slice(1),
    page_view: parsed_input.args[0],
    public_fsRoot: "public",
  };
}

import { join } from "https://deno.land/std@0.224.0/path/mod.ts";

export async function import_page_view(filename: string) {
  return await import(
    "file://" + join(Deno.cwd(), filename)
  )
}