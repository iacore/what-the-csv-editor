import render from "https://esm.sh/preact-render-to-string@6.4.2";
import {
  type ParsedCSVData,
  readCSVDataMulti,
  writeCSVData,
} from "./csv.ts";
import { transpile } from "https://deno.land/x/emit@0.40.0/mod.ts";
import type { UpdateEventData } from "./types.ts";
import { type Config, get_config, import_page_view } from "./config.ts";
import { serveDir } from "https://deno.land/std@0.224.0/http/mod.ts";
import { assert } from "https://deno.land/std@0.224.0/assert/mod.ts";

function process_update_event(
  dataset: Record<string, ParsedCSVData>,
  event: UpdateEventData,
) {
  const data = dataset[event.dataset];
  assert(data, "dataset not found: " + event.dataset);
  const obj = data.objects[event.index];
  if (typeof obj !== "object") throw new Error("wtf");
  obj[event.changed_key] = event.changed_value;
}

import { Mutex, Notify } from "https://deno.land/x/async@v2.1.0/mod.ts";

async function runServer(config: Config) {
  const mutex_rw = new Mutex();
  let dataset: Record<string, ParsedCSVData>;
  async function reload() {
    await mutex_rw.acquire();
    try {
      dataset = await readCSVDataMulti(config.data_files);
    }
    finally {
      mutex_rw.release();
    }
  }

  const sig_wait = new Notify();
  const save_thread_args: Record<string, ParsedCSVData> = {};
  async function save() {
    await sig_wait.notified();
    await mutex_rw.acquire();
    try {
      while (true) {
        const keys = Object.keys(save_thread_args);
        if (keys.length > 0) {
          const data = save_thread_args[keys[0]];
          delete save_thread_args[keys[0]];
          await writeCSVData(keys[0], data);
        } else {
          break;
        }
      }
    } finally {
      mutex_rw.release();
    }
    save();
  }
  save();

  const { doctype, IndexPage } = await import_page_view(config.page_view);

  Deno.serve(async (req) => {
    const url = new URL(req.url);
    // console.log(url);
    if (url.pathname == "/") {
      await reload();
      const body = (doctype ?? "<!DOCTYPE html>") +
        render(IndexPage({ dataset, editing: true }));
      return new Response(body, {
        headers: {
          "content-type": "text/html",
        },
      },
      );
    } else if (url.pathname == "/edit.js") {
      // console.log(import.meta.filename);
      const url = new URL("./edit.ts", import.meta.url);
      const result = await transpile(url);

      const code = result.get(url.href);

      return new Response(
        code,
        {
          headers: {
            "content-type": "application/javascript",
          },
        },
      );
    } else if (url.pathname == "/update-object") {
      const event = await req.json() as UpdateEventData;
      process_update_event(dataset, event);
      save_thread_args[event.dataset] = dataset[event.dataset];
      sig_wait.notify();
      return new Response("", {
        status: 200,
      });
    } else {
      return serveDir(req, {
        fsRoot: config.public_fsRoot,
      });
    }
  });
}


const config = await get_config();
runServer(config);
